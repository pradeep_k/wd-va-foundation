﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-28 13:21:23

using System;
using System.Collections;
using System.Collections.Generic;
using Framework.UI.Window;
using Framework.Responders;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Framework.UI.Culling;
using Framework.UI.Editor;

namespace Framework.UI
{
    internal sealed class UIWindow : MonoBehaviour, IResponder, IPresentVCEventHandler
    {
        #if LOG_VIEW_ARCH_VERBOSE
        public const bool LOG_VERBOSE = true;
        #else
        public const bool LOG_VERBOSE = false;
        #endif

        // editor
        [SerializeField] Canvas m_Canvas;
        [SerializeField] UIViewController m_RootViewController;

        public IResponder Super => null;

        public void OnPresentViewController(
            UIViewController vc, 
            UIViewController presenter, 
            UIPresentationStyle style, 
            UITransitionStyle transistion)
        {
            // present vc as modal 
            vc.WillMove(null);
            vc.SetPresentingParent(presenter);
            presenter?.SetPresented(vc);
            vc.DidMove(null);

            vc.SetParentWindow(this);

            // add to the view canvas
            // m_ModalViewVoidBackground.transform.SetAsLastSibling();
            vc.RootView.transform.SetParent(m_Canvas.transform);
            vc.RootView.transform.SetAsLastSibling();
            vc.RootView.gameObject.SetActive(true);

            // rect correction
            RectTransform vc_rect_transform = vc.RootView.gameObject.GetComponent<RectTransform>();
            if(vc_rect_transform != null)
                CorrectRect(vc_rect_transform);
            
            // apply correct presentation style
            vc.RootView.Style = style == UIPresentationStyle.None ? vc.RootView.Style : style; // if no preference use what is on editor
            vc.RootView.Style = vc.RootView.Style == UIPresentationStyle.None ? UIPresentationStyle.FullScreen : vc.RootView.Style; // if None is selected in editor default to Fullscreen

             // transition
            vc.OnViewWillAppear();
            presenter?.ChildWillShow(vc);

            // look for editor components
            if(transistion == UITransitionStyle.UseEditorValue)
                transistion = GetTransitionStyle(vc, true);

            // apply transition direction flag only if user has transition
            transistion = !transistion.HasFlag(UITransitionStyle.DoNotAnimate) ? transistion | UITransitionStyle.TransitIn : UITransitionStyle.DoNotAnimate;
            _TransitionManager.Transition(vc.RootView, transistion, 4f, () => 
            {
                EventSystem.current.SetSelectedGameObject(vc.RootView.gameObject);
                vc.OnViewDidAppear();
                presenter?.ChildDidShow(vc);
            });
            
            _CurrentModal = vc;
        }

        public void OnAddChildViewController(
            UIViewController vc, 
            UIViewController presenter, 
            UIPresentationStyle style, 
            UITransitionStyle transistion)
        {
            vc.SetParentWindow(null);

            vc.WillMove(presenter);
            vc.SetPresentingParent(presenter);
            presenter.SetPresented(vc);
            vc.DidMove(presenter);

            // add to the view canvas
            vc.RootView.transform.SetParent(presenter.RootView.ChildRoot);
            vc.RootView.transform.SetAsLastSibling();
            vc.RootView.gameObject.SetActive(true);

            // rect correction
            RectTransform vc_rect_transform = vc.RootView.gameObject.GetComponent<RectTransform>();
            if(vc_rect_transform != null)
                CorrectRect(vc_rect_transform);

            // apply correct presentation style
            vc.RootView.Style = style == UIPresentationStyle.None ? vc.RootView.Style : style; // if no preference use what is on editor
            vc.RootView.Style = vc.RootView.Style == UIPresentationStyle.None ? UIPresentationStyle.FullScreen : vc.RootView.Style; // if None is selected in editor default to Fullscreen

             // transition
            vc.OnViewWillAppear();
            presenter.ChildWillShow(vc);

            // look for editor components
            if(transistion == UITransitionStyle.UseEditorValue)
                transistion = GetTransitionStyle(vc, true);
                
            transistion = !transistion.HasFlag(UITransitionStyle.DoNotAnimate) ? transistion | UITransitionStyle.TransitIn : UITransitionStyle.DoNotAnimate;
            _TransitionManager.Transition(vc.RootView, transistion, 4f, () =>
            {
                EventSystem.current.SetSelectedGameObject(vc.RootView.gameObject);

                vc.OnViewDidAppear();
                presenter.ChildDidShow(vc);
            });
        }

        public void OnDismissViewController(UIViewController vc, UIViewController presenter, UITransitionStyle transistion)
        {
            vc.SetParentWindow(null);

            // present vc as modal 
            vc.WillMove(null);
            vc.SetPresentingParent(null);
            presenter.SetPresented(null);
            vc.DidMove(null);
            vc.transform.SetParent(_DismissedViewControllers);
            vc.transform.SetAsFirstSibling();

             // transition
            vc.OnViewWillDisappear();
            presenter.ChildWillDismiss(vc);

            // look for editor components
            if(transistion == UITransitionStyle.UseEditorValue)
                transistion = GetTransitionStyle(vc, false);

            transistion = !transistion.HasFlag(UITransitionStyle.DoNotAnimate) ? transistion & ~UITransitionStyle.TransitIn : UITransitionStyle.DoNotAnimate;
            _TransitionManager.Transition(vc.RootView, transistion, 4f, () =>
            {
                EventSystem.current.SetSelectedGameObject(presenter.RootView.gameObject);

                // remove from view canvas
                vc.RootView.transform.SetParent(_DisappearedViews);
                vc.RootView.transform.SetAsFirstSibling();
                vc.RootView.gameObject.SetActive(false);

                vc.OnViewDidDisappear();
                presenter.ChildDidDismiss(vc);
            });
        }

        // internal
        /// <summary>
        /// Gets the top most view
        /// </summary>
        internal UIView TopView { get{ return  m_Canvas.transform.childCount > 0 ? 
                                                m_Canvas.transform.GetChild(m_Canvas.transform.childCount - 1).GetComponent<UIView>() 
                                                : null; }}

        // private
        UIViewTransitionManager _TransitionManager;
        UIViewController _CurrentModal;
        Transform _DisappearedViews;
        Transform _DismissedViewControllers;

        UIView _PrevFocusedView;

        // Start is called before the first frame update
        void Start()
        {   
            // attach presenter if it is not present
            _TransitionManager = m_Canvas.GetComponent<UIViewTransitionManager>();
            if(_TransitionManager == null)
                _TransitionManager = m_Canvas.gameObject.AddComponent<UIViewTransitionManager>();

            // attach culling if it is not present
            if(m_Canvas.GetComponent<UIViewCulling>() == null)
                m_Canvas.gameObject.AddComponent<UIViewCulling>();

            // discard piles
            _DisappearedViews           = new GameObject("disappeared-views").transform;
            _DismissedViewControllers   = new GameObject("dismissed-view-controllers").transform;
           
            // root vc
            OnPresentViewController(m_RootViewController, null, UIPresentationStyle.FullScreen, UITransitionStyle.UseEditorValue);
        }
        
        void Update()
        {
            UIView _top_of_the_stack_view = TopView;
            if(_top_of_the_stack_view != null // FIX* TopView might return null if in same frame an object moved from disabled parent
                && _PrevFocusedView != _top_of_the_stack_view)
            {
                _PrevFocusedView?.OnFocus(false);
                _top_of_the_stack_view?.OnFocus(true);
                if(LOG_VERBOSE) Debug.Log("UIWindow: Detected root level view focus change, in: "+_top_of_the_stack_view+", out: "+_PrevFocusedView+", f: "+Time.frameCount);

                _PrevFocusedView = _top_of_the_stack_view;
            }
        }

        /// <summary>
        /// Resets the rect to stretched transform values
        /// </summary>
        /// <param name="rectTransform"></param>
        void CorrectRect(RectTransform rectTransform)
        {
            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
            rectTransform.localPosition = Vector3.zero;
            rectTransform.localScale = Vector3.one;
        }   
        
        UITransitionStyle GetTransitionStyle(UIViewController vc, bool transit_in)
        {
            foreach (var item in vc.GetComponents<UITransition>())
            {
                if(item.Transit == (transit_in ? UITransition.Direction.In :  UITransition.Direction.Out))
                    return item.ToStyle();
            }
            return UITransitionStyle.UseEditorValue;
        }
    }

    public interface IPresentVCEventHandler : IWindowEventHandler
    {
        void OnPresentViewController(UIViewController vc, UIViewController presenter, UIPresentationStyle style, UITransitionStyle transition);
        void OnAddChildViewController(UIViewController vc, UIViewController presenter, UIPresentationStyle style, UITransitionStyle transition);
        void OnDismissViewController(UIViewController vc, UIViewController presenter, UITransitionStyle transition);
    }

}