//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-23 16:23:22

using UnityEngine.EventSystems;

namespace Framework.UI.Window
{
    public interface IWindowEventHandler : IEventSystemHandler{}

    public class WindowEventData : BaseEventData
    {
        public UIViewController sender;
        
        public WindowEventData(EventSystem eventSystem, UIViewController sender) : base(eventSystem)
        {
            this.sender = sender;
        }
    }
}