//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-23 17:22:45

using System;

namespace Framework.UI
{
    [Flags]
    public enum UITransitionStyle : long
    {
        // 0000 000F
        UseEditorValue  = 0,            
        DoNotAnimate    = 1,

        // direction
        // 0000 00F0
        TransitIn       = 2,            

        // types
        // FFFF FF00
        Fade            = 8,
        Explode         = 16,
        Implode         = 32,
        SlideRight      = 64,
        SlideLeft       = 128,
        SlideUp         = 256,
        SlideDown       = 512,
        
        // curve style
        // Note* no curve flag leads to linear
        EaseIn          = 1024,
        EaseOut         = 2048
    }
}