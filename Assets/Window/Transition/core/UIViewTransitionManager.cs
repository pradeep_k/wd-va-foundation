//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-23 18:55:09

#define LOG_VIEW_TRANSITION_MANAGER

using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Animate;
using Framework.UI;
using Framework.UI.Animate;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.UI
{
    // TODO* Add support to transit with time
    internal class UIViewTransitionManager : MonoBehaviour
    {
        #if LOG_VIEW_TRANSITION_MANAGER
            public const bool LogVerbose = true;
        #else
            public const bool LogVerbose = false;
        #endif

        internal static UIViewTransitionManager Instance { get; private set; }

        internal bool IsTransitionInProgress(UIView view)
        {
            return m_transitions.ContainsKey(view);
        }

        // internals
        internal bool Transition(
            UIView view, 
            UITransitionStyle transition,
            float rate,
            Action Completed)
        {
            // update look up
            if(!m_transitions.ContainsKey(view))
                m_transitions.Add(view, null);

            // release existing animations
            if(m_transitions[view] != null)
            {
                Debug.Log("UITransitionManager: Transition interrupted for view: "+view);
                Release(view);
            }

             // blocker ui interaction until transition is over
             if(m_interaction_blocker == null)
             {
                m_interaction_blocker = new GameObject("Transition-Interaction-Blocker").AddComponent<Image>();
                m_interaction_blocker.gameObject.transform.SetParent(transform);
                m_interaction_blocker.rectTransform.anchoredPosition = Vector2.zero;
                m_interaction_blocker.rectTransform.anchorMin        = new Vector2(1, 0);
                m_interaction_blocker.rectTransform.anchorMax        = new Vector2(0, 1);
                m_interaction_blocker.rectTransform.pivot            = new Vector2(0.5f, 0.5f);
                m_interaction_blocker.rectTransform.offsetMax        = Vector2.zero;
                m_interaction_blocker.rectTransform.offsetMin        = Vector2.zero;
                m_interaction_blocker.color                          = new Color();
             }
            
            // start transition
            m_transitions[view] = Prepare(view, transition, rate);
            if(LogVerbose) Debug.Log("UITransitionManager: Starting Animations for view: "+view);
            Play(m_transitions[view], transition, () =>
            {
                if(LogVerbose) 
                    Debug.Log("UITransitionManager: Animations Completed for view: "+view
                                +"\n\t animations: "+m_transitions[view].Count);

                Release(view);
                m_transitions.Remove(view);

                // release interaction blocker if all transitions are done
                if(m_transitions.Count == 0 && m_interaction_blocker != null && !m_interaction_blocker.IsDestroyed())
                    Destroy(m_interaction_blocker.gameObject);

                Completed?.Invoke();
            });
           
            return false;
        }

        // privates
        Image m_interaction_blocker;
        // all the transitions currently in progress
        Dictionary<UIView, List<AnimateBase>> m_transitions = new Dictionary<UIView, List<AnimateBase>>();

        void Awake()
        {
            Instance = this;
        }

        List<AnimateBase> Prepare(UIView view, UITransitionStyle transition, float rate)
        {
            var list = new List<AnimateBase>();

            if(transition == UITransitionStyle.DoNotAnimate)
                return list;
            
            string log = "";
            bool transit_in = transition.HasFlag(UITransitionStyle.TransitIn);
            log += "\tTransit: "+ (transit_in ? "in" : "out");
            log += "\tRate: (units per sec)" + rate;
            
            // setup animate component
            // fade
            if(transition.HasFlag(UITransitionStyle.Fade))
            {
                AnimateCanvasGroupAlpha animate = Get<AnimateCanvasGroupAlpha>(view, rate);
                animate.From    = transit_in ? 0 : 1;
                animate.To      = transit_in ? 1 : 0;
                list.Add(animate);

                log += "\n\tAnimate: "+UITransitionStyle.Fade;
            }

            // scale
            if(transition.HasFlag(UITransitionStyle.Explode))
            {
                AnimateScale animate = Get<AnimateScale>(view, rate);
                
                animate.From    = transit_in ? new Vector2(0.5f, 0.5f) : Vector2.one;
                animate.To      = transit_in ? Vector2.one : new Vector2(1.5f, 1.5f);
                list.Add(animate);

                log += "\n\tAnimate: "+UITransitionStyle.Explode;
            }else
            if(transition.HasFlag(UITransitionStyle.Implode))
            {
                AnimateScale animate = Get<AnimateScale>(view, rate);
                
                animate.From    = transit_in ? new Vector2(1.5f, 1.5f) : Vector2.one;
                animate.To      = transit_in ? Vector2.one : new Vector2(0.5f, 0.5f);
                list.Add(animate);

                log += "\n\tAnimate: "+UITransitionStyle.Explode;
            }

            // slide
            if(transition.HasFlag(UITransitionStyle.SlideRight))
            {
                AnimateAnchorPosition animate = Get<AnimateAnchorPosition>(view, rate);
                float width = view.GetComponent<RectTransform>().rect.size.x;
                Vector2 position = view.GetComponent<RectTransform>().anchoredPosition;

                animate.From    = transit_in ? new Vector2(-width, position.y) : new Vector2(0, position.y) ;
                animate.To      = transit_in ? new Vector2(0, position.y) : new Vector2(width, position.y);
                list.Add(animate);

                log += "\n\tAnimate: "+UITransitionStyle.SlideRight;
            }
            else if(transition.HasFlag(UITransitionStyle.SlideLeft))
            {
                AnimateAnchorPosition animate = Get<AnimateAnchorPosition>(view, rate);
                float width = view.GetComponent<RectTransform>().rect.size.x;
                Vector2 position = view.GetComponent<RectTransform>().anchoredPosition;

                animate.From    = transit_in ? new Vector2(width, position.y) : new Vector2(0, position.y);
                animate.To      = transit_in ? new Vector2(0, position.y) : new Vector2(-width, position.y);
                list.Add(animate);

                log += "\n\tAnimate: "+UITransitionStyle.SlideLeft;
            }

            if(transition.HasFlag(UITransitionStyle.SlideUp))
            {
                AnimateAnchorPosition animate = Get<AnimateAnchorPosition>(view, rate);
                float height = view.GetComponent<RectTransform>().rect.size.y;
                Vector2 position = view.GetComponent<RectTransform>().anchoredPosition;

                animate.From    = transit_in ? new Vector2(position.x, -height) : new Vector2(position.x, 0) ;
                animate.To      = transit_in ? new Vector2(position.x, 0)  :new Vector2(position.x, height);
                list.Add(animate);

                log += "\n\tAnimate: "+UITransitionStyle.SlideUp;
            }
            else if(transition.HasFlag(UITransitionStyle.SlideDown))
            {
                AnimateAnchorPosition animate = Get<AnimateAnchorPosition>(view, rate);
                float height = view.GetComponent<RectTransform>().rect.size.y;
                Vector2 position = view.GetComponent<RectTransform>().anchoredPosition;

                animate.From    = transit_in ? new Vector2(position.x, height) : new Vector2(position.x, 0) ;
                animate.To      = transit_in ? new Vector2(position.x, 0)  :new Vector2(position.x, -height);
                list.Add(animate);

                log += "\n\tAnimate: "+UITransitionStyle.SlideDown;
            }

            if(LogVerbose) Debug.Log("UITransitionManager: Prepared transition, "+view+"\n"
                                                                                    +log+"\n");
            return list;
        }

        void Play(List<AnimateBase> animations, UITransitionStyle transition, Action complete)
        {
            if(animations.Count == 0)
            {
                complete?.Invoke();
                return;
            }

            // set correct curve
            Func<float, float> curve = null;
            if(transition.HasFlag(UITransitionStyle.EaseIn) && transition.HasFlag(UITransitionStyle.EaseOut))
            {
                curve = CurveFunctions.EaseInOutCubic;
            }
            else if(transition.HasFlag(UITransitionStyle.EaseIn))
            {
                curve = CurveFunctions.EaseInCubic;
            }
            else if(transition.HasFlag(UITransitionStyle.EaseOut))
            {
                curve = CurveFunctions.EaseOutCubic;
            }
            else
            {
                // default to linear
            }

            int count = 0;
            animations.ForEach( x => 
            {
                x.OnComplete = (f) => 
                {
                    count++;
                    if(count == animations.Count)
                    {
                        complete?.Invoke();
                    }
                };
                x.Tween(curve);
            });
        }

        void Release(UIView view)
        {
            string log = "\tview: "+view;
            m_transitions[view].ForEach( x => { log = log + x.ToString() +"\n"; Destroy(x); } );
            m_transitions[view] = null;
            if(LogVerbose) Debug.Log("UITransitionManager: Releasing resources\n\n"+log);
        }

        T Get<T>(UIView view, float rate) where T : AnimateBase
        {
            T result = view.gameObject.AddComponent<T>();
            result.Rate = rate;
            result.enabled = false;
            return result;
        }
    }
}