﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-28 17:32:28

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.UI.Editor
{
    public class UITransitionBase : MonoBehaviour
    {
        // editor
        public Direction Transit;

        // nested
        public enum Direction
        {
            In,
            Out
        }
    }
}
