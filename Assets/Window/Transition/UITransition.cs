//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-28 17:32:24

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.UI.Editor
{
    public class UITransition : UITransitionBase
    {
        // editor
        public SupportedCurves      Curve;
        public InbuiltTypes[]       Type;

        // nested
        public enum InbuiltTypes
        {
            Fade,
            Explode,
            Implode,
            SlideRight,
            SlideLeft,
            SlideUp,
            SlideDown,
        }
        public enum SupportedCurves
        {
            Linear,
            EaseIn,
            EaseOut,
            EaseInOut
        }

        public UITransitionStyle ToStyle()
        {
            UITransitionStyle _style = UITransitionStyle.UseEditorValue;

            switch (Curve)
            {
                case SupportedCurves.EaseIn:
                    _style = _style | UITransitionStyle.EaseIn;
                    break;
                case SupportedCurves.EaseOut:
                    _style = _style | UITransitionStyle.EaseOut;
                    break;
                case SupportedCurves.EaseInOut:
                    _style = _style | UITransitionStyle.EaseIn | UITransitionStyle.EaseOut;
                    break;
            }

            foreach (var item in Type)
            {
                switch (item)
                {
                    case InbuiltTypes.Fade:
                        _style = _style | UITransitionStyle.Fade;
                        break;
                    case InbuiltTypes.Explode:
                        _style = _style | UITransitionStyle.Explode;
                        break;
                    case InbuiltTypes.Implode:
                        _style = _style | UITransitionStyle.Implode;
                        break;
                    case InbuiltTypes.SlideRight:
                        _style = _style | UITransitionStyle.SlideRight;
                        break;
                    case InbuiltTypes.SlideLeft:
                        _style = _style | UITransitionStyle.SlideLeft;
                        break;
                    case InbuiltTypes.SlideDown:
                        _style = _style | UITransitionStyle.SlideDown;
                        break;
                    case InbuiltTypes.SlideUp:
                        _style = _style | UITransitionStyle.SlideUp;
                        break;
                }
            }

            return _style;
        }
    }
}
