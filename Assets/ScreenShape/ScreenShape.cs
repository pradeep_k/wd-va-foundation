﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-12 14:21:25

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Framework.UI
{
    // [ExecuteAlways]
    public partial class ScreenShape : MonoBehaviour
    {
        public static ScreenShape Instance { get{

                                                if(m_Instance == null)
                                                    m_Instance = FindObjectOfType<ScreenShape>();

                                                return m_Instance; } }

        //editor
        public bool DonotDestroyOnLoad = false;
        public UnityAction<Shape> OnChange;

        public Shape Current => m_Aspect > 1f ? Shape.Wide : Shape.Tall;
        public float Aspect => m_Aspect;

        // private
        static ScreenShape m_Instance;
        float m_Aspect = -1;

        [ContextMenu("ReInit")]
        void Awake()
        {
            m_Instance = this;

            if(Application.isPlaying)
            {
                if(DonotDestroyOnLoad)
                    DontDestroyOnLoad(this.gameObject);
            }
        }

        void Update()
        {
            Vector2 dimensions = GetScreenDimensions();
            float aspect = dimensions.x/dimensions.y;
            if(m_Aspect != aspect)
            {
                m_Aspect = aspect;
                OnChange?.Invoke(Current);
                Debug.Log("ScreenShape: Changed: "+Current);
            }
        }

        /// <summary>
        /// Orientation independent dimensions
        /// </summary>
        /// <returns></returns>
        Vector2 GetScreenDimensions()
        {
            return new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);
        }
    }
}