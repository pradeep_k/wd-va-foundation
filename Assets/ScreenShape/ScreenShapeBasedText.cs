﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-12 14:21:25

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static Framework.UI.ScreenShape;
using static UnityEngine.RectTransform;

namespace Framework.UI
{
    public class ScreenShapeBasedText: ScreenShapeBasedBase
    {
        // editor
        [SerializeField]
        TargetTextInfo[] m_Targets;
        
        protected override void OnShapeChange(Shape shape)
        {
            if(LogVerbose) Debug.Log("ScreenShapeBasedText: On Shape Change: " +shape+", gameobject: "+gameObject.name);
            TargetTextInfo target_text_info = System.Array.Find(m_Targets, (x) => x.Shape == shape);
            if (target_text_info != null)
            {
                if(LogVerbose) Debug.Log("ScreenShapeBasedText: Found target for shape: " +shape+", gameobject: "+gameObject.name);
                Text text = GetComponent<Text>();
                if (text != null)
                {
                    text.alignment = target_text_info.Align;
                }
            }
        }

        // nested class
        [System.Serializable]
        public class TargetTextInfo : TargetInfo
        {
            public TextAnchor Align;
        }
    }
}