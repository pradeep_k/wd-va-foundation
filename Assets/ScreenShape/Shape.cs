//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-12 14:21:25

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Framework.UI
{
    public partial class ScreenShape
    {
        // nested
        public enum Shape
        {
            Wide,
            Tall,

            UNDEFINED = -1
        }
    }
}