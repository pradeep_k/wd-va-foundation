//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-12 14:21:25

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using static Framework.UI.ScreenShape;
using static UnityEngine.RectTransform;

namespace Framework.UI
{
    public class ScreenShapeBasedTransform : ScreenShapeBasedBase
    {
        // editor
        [SerializeField]
        TargetRectInfo[] m_Targets;

#if UNITY_EDITOR
        public void OnEditorShapeChange(Shape shape)
        {
            OnShapeChange(shape);
        }
#endif

        protected override void OnShapeChange(Shape shape)
        {
            if(LogVerbose) Debug.Log("ScreenShapeBasedTransform: On Shape Change: " +shape+", gameobject: "+gameObject.name);
            TargetRectInfo targetRectInfo = System.Array.Find(m_Targets, (x) => x.Shape == shape);
            if (targetRectInfo != null)
            {
                if(LogVerbose) Debug.Log("ScreenShapeBasedTransform: Found transform shape: " +shape+", gameobject: "+gameObject.name);

                transform.parent = targetRectInfo.PlaceHolderParent;

                RectTransform rectTransform = GetComponent<RectTransform>();
                if (rectTransform != null)
                {
                    //Note* order of assigning matters here
                    // rectTransform.anchoredPosition  = targetRectInfo.AnchoredPosition;

                    // force stretch
                    rectTransform.anchoredPosition  = Vector2.zero;
                    rectTransform.pivot             = new Vector2(0.5f, 0.5f);
                    rectTransform.offsetMin         = Vector2.zero;
                    rectTransform.offsetMax         = Vector2.zero;

                    // rectTransform.pivot             = targetRectInfo.Pivot;
                    // rectTransform.offsetMin         = targetRectInfo.OffsetMin;
                    // rectTransform.offsetMax         = targetRectInfo.OffsetMax;

                    // bool is_horizontally_stretched  = Mathf.Abs(rectTransform.offsetMin.x - rectTransform.offsetMax.x) == 1;
                    // bool is_vertically_stretched    = Mathf.Abs(rectTransform.offsetMin.y - rectTransform.offsetMax.y) == 1;
                    // if horizontal stretch is enabled
                    // if(is_horizontally_stretched)
                    //     rectTransform.SetSizeWithCurrentAnchors(Axis.Horizontal, targetRectInfo.Dimensions.x);

                    // // if vertical stretch is enabled
                    // if(is_vertically_stretched)
                    //     rectTransform.SetSizeWithCurrentAnchors(Axis.Vertical, targetRectInfo.Dimensions.y);

                    // non stretched dimensions
                    // Vector2 dimensions = new Vector2(
                    //                                     is_horizontally_stretched ? rectTransform.sizeDelta.x : targetRectInfo.Dimensions.x,
                    //                                     is_vertically_stretched ? rectTransform.sizeDelta.y : targetRectInfo.Dimensions.y
                    //                                 );
                    // rectTransform.sizeDelta         = dimensions;
                }
            }
        }

        // nested class
        [System.Serializable]
        public class TargetRectInfo
        {
            // public Vector3  AnchoredPosition;
            // public Vector2  OffsetMin;
            // public Vector2  OffsetMax;
            // public Vector2  Pivot;
            // public Vector2  Dimensions;
            public RectTransform PlaceHolderParent;
            public Shape Shape;
        }
    }
}