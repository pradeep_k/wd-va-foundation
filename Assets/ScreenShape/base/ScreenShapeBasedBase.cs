//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-12 14:21:25

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using static Framework.UI.ScreenShape;
using static UnityEngine.RectTransform;

namespace Framework.UI
{
    // [ExecuteAlways]
    public class ScreenShapeBasedBase : MonoBehaviour
    {
        // editor
        [SerializeField]
        protected bool LogVerbose = false;

        // private
        ScreenShape.Shape m_prev_shape = ScreenShape.Shape.UNDEFINED;

        protected virtual void Update()
        {
            if(ScreenShape.Instance != null && m_prev_shape != ScreenShape.Instance.Current)
            {
                m_prev_shape = ScreenShape.Instance.Current;
                OnShapeChange(m_prev_shape);
            }
        }

        protected virtual void OnShapeChange(Shape shape){}

    }

    [System.Serializable]
    public class TargetInfo
    {
        public Shape Shape;
    }
}