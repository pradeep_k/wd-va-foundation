﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:10:26

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Framework.UI
{
    /// <summary>
    /// Will dismiss the view controller when a particular key is down
    /// </summary>
    public class UIPopNavigationVConKeyDown : MonoBehaviour
    {
        //editor
        public KeyCode Key = KeyCode.Escape;

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(Key))
                GetComponent<UINavigationController>().Pop();
        }
    }
}