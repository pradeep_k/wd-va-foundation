//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-30 14:20:42

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Interaction
{
    public class UIFlick : MonoBehaviour,
    IBeginDragHandler,
    IEndDragHandler,
    IDragHandler
    {
        //editor
        public bool BubbleUnityDragEvents = false;

        // interaction
        public void OnBeginDrag(PointerEventData eventData)
        {
            m_momentum_vec = m_momentum_vec + eventData.delta/m_screen_res_vec;

            // Debug.Log("UIFlick: OnBeginDrag, momentum: "+ m_momentum_vec+"f: "+Time.frameCount); // debug only*
            if(BubbleUnityDragEvents && transform.parent != null)
                ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.beginDragHandler);
        }
        public void OnDrag(PointerEventData eventData)
        {
            m_momentum_vec = m_momentum_vec + eventData.delta/m_screen_res_vec;

            //  Debug.Log("UIFlick: OnDrag, momentum: "+ m_momentum_vec+"f: "+Time.frameCount); // debug only*
            if(BubbleUnityDragEvents && transform.parent != null)
                ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.dragHandler);
        }
        public void OnEndDrag(PointerEventData eventData)
        {
            m_momentum_vec = m_momentum_vec + eventData.delta/m_screen_res_vec;

            // Debug.Log("UIFlick: OnEndDrag, momentum: "+ m_momentum_vec+" f: "+Time.frameCount); // debug only*
            if(enabled == true)
            {
                if(m_momentum_vec.magnitude < THRESHOLD)
                    return;

                    // Debug.Log("UIFlick: " + gameObject.name + ": Flicked"
                    //                                         + "\n\tmomentum, " + m_momentum_vec); // debug only*
                                                            

                    WDPointerEventData data = new WDPointerEventData(gameObject, eventData);
                    ExecuteEvents.Execute<IFlickEventHandler>(gameObject, data, (x, y) => { x.OnFlick(m_momentum_vec.normalized, data); });
            }

            if(BubbleUnityDragEvents && transform.parent != null)
                ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.endDragHandler);
            
            m_momentum_vec = Vector2.zero;
        }

        // private
        const float THRESHOLD = 0.025f;
        Vector2 m_momentum_vec;
        Vector2 m_screen_res_vec;
        Vector2 m_loss_per_sec_vec = new Vector2(0.5f, 0.5f);
        ScreenOrientation m_prev_orientation;
        void OnEnable()
        {
            m_momentum_vec      = Vector2.zero;
        }

        void Update()
        {
            if(m_prev_orientation != Screen.orientation)
            {
                m_screen_res_vec    = new Vector2(Screen.width, Screen.height);
                m_prev_orientation = Screen.orientation;
            }

            if(m_momentum_vec.magnitude > 0.01f)
            {
                Vector2 _dir = new Vector2(Mathf.Sign(m_momentum_vec.x), Mathf.Sign(m_momentum_vec.y));
                Vector2 _directional_loss_per_sec_frame = m_loss_per_sec_vec * -_dir * Time.deltaTime;

                m_momentum_vec = m_momentum_vec + _directional_loss_per_sec_frame;
                m_momentum_vec.x = _dir.x >= 0 ? Mathf.Clamp(m_momentum_vec.x, 0 , 1) : Mathf.Clamp(m_momentum_vec.x, -1 , 0);
                m_momentum_vec.y = _dir.y >= 0 ? Mathf.Clamp(m_momentum_vec.y, 0 , 1) : Mathf.Clamp(m_momentum_vec.y, -1 , 0);

                // Debug.Log("UIFlick: Update, momentum: "+ m_momentum_vec+" f: "+Time.frameCount); // debug only*
            }
        }
    }
}