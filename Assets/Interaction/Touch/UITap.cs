//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-30 14:20:42

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Interaction
{
    public class UITap : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            m_start_time = DateTime.Now;
            m_start_pos = eventData.position;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if(enabled != true)
                return;

            Vector2 _ndc_vec        = GetNDC(eventData.position - m_start_pos);
            _ndc_vec                = new Vector2(Mathf.Abs(_ndc_vec.x), Mathf.Abs(_ndc_vec.y));
            float   _drag_distance  = _ndc_vec.x > _ndc_vec.y ? _ndc_vec.x : _ndc_vec.y;

            if((DateTime.Now - m_start_time).TotalMilliseconds < (TAP_TIME_OUT_SEC * 1000) && 
                _drag_distance < TAP_DRAG_THRESHOLD_NDC)
            {
                // Debug.Log("UITapResponse: "+gameObject.name +": Tapped"
                //                                             +"\n\tHeld (msec), "+(DateTime.Now - m_start_time).Milliseconds
                //                                             +"\n\tDragged (ndc), "+_drag_distance); // debug only*

                WDPointerEventData data = new WDPointerEventData(gameObject, eventData);
                ExecuteEvents.Execute<ITapEventHandler>(gameObject, data, (x, y) => { x.OnTap(data); });
            }
        }

        // private
        const float TAP_TIME_OUT_SEC = 0.3f;
        const float TAP_DRAG_THRESHOLD_NDC = 0.01f;

        DateTime m_start_time = new DateTime();
        Vector2 m_start_pos;

        Vector2 GetNDC(Vector2 vec)
        {
            return new Vector2(vec.x/Screen.currentResolution.width, vec.y/Screen.currentResolution.height);
        }
    }
}