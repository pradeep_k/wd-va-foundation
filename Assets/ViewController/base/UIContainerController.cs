//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-27 14:29:49

using System;
using Framework.EventSystems;
using Framework.Responders;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.UI
{
    public class UIContainerController : UIViewController, IDismissParentContainerEventHandler, IAddChildToContainer
    {
        protected override void Awake()
        {
            base.Awake();
            UIViewController[] _child_vcs = GetComponentsInChildren<UIViewController>();
            // setup the responder chain vc hierarchy for editor setup in editor
            Array.ForEach(_child_vcs, vc =>
            {
                if(vc == this) // exclude parent
                    return;

                vc.SetParentWindow(null);

                vc.WillMove(this);
                vc.SetPresentingParent(this);
                SetPresented(vc);
                vc.DidMove(this);
            });
        }

        /// <summary>
        /// Present view as a child
        /// </summary>
        /// <param name="vc"></param>
        protected virtual void AddChild(UIViewController vc, 
                                        UITransitionStyle transition = UITransitionStyle.UseEditorValue, 
                                        UIPresentationStyle style = UIPresentationStyle.None)
        {
            WDBaseEventData data = new WDBaseEventData(this.gameObject);
            SendEventViaResponderChain<IPresentVCEventHandler>(data, (x, y) => { x.OnAddChildViewController(vc, this, style, transition); });
        }

        public void OnDismissParentContainer(WDBaseEventData data)
        {
            Dismiss();
        }

        public void OnAddChildViewController(BaseEventData data, UIViewController vc)
        {
            AddChild(vc);
        }
    }
}