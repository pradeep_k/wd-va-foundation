//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-27 14:29:44

using System;
using Framework.EventSystems;
using Framework.Responders;
using UnityEngine;
using UnityEngine.EventSystems;
using static UnityEngine.EventSystems.ExecuteEvents;

namespace Framework.UI
{
    public class UIViewController : MonoBehaviour, IResponder, ICloseEventHandler
    {
        // public
        #if LOG_VIEW_ARCH_VERBOSE
        public const bool LOG_VERBOSE = true;
        #else
        public const bool LOG_VERBOSE = false;
        #endif

        public UIView RootView;
        public IResponder Super => ParentViewController ?? (IResponder) _ParentWindow;
        public UIViewController ParentViewController => _ParentViewController;
        
        /// <summary>
        /// Present a new vc
        /// </summary>
        /// <param name="vc"></param>
        public virtual void Show(UIViewController vc, 
                                    UITransitionStyle transition = UITransitionStyle.UseEditorValue, 
                                    UIPresentationStyle style = UIPresentationStyle.None)
        {
            PresentAsModal(vc, transition, style);
        }

        /// <summary>
        /// Present a new vc as modal
        /// </summary>
        /// <param name="vc"></param>
        public void PresentAsModal(UIViewController vc, 
                                    UITransitionStyle transition = UITransitionStyle.UseEditorValue, 
                                    UIPresentationStyle style = UIPresentationStyle.None)
        {
            WDBaseEventData data = new WDBaseEventData(this.gameObject);
            SendEventViaResponderChain<IPresentVCEventHandler>(data, (x,y) =>{ x.OnPresentViewController(vc, this, style, transition); } );
        }

        /// <summary>
        /// Dismiss the presented vc
        /// </summary>
        public void DismissPresented(UITransitionStyle transition = UITransitionStyle.UseEditorValue)
        {
            WDBaseEventData data = new WDBaseEventData(this.gameObject);
            SendEventViaResponderChain<IPresentVCEventHandler>(data, (x,y) =>{ x.OnDismissViewController(PresentedView, this, transition); } );
        }

        /// <summary>
        /// Dismiss self
        /// </summary>
        public virtual void Dismiss(UITransitionStyle transition = UITransitionStyle.UseEditorValue)
        {
            ParentViewController?.DismissPresented(transition);
        }

        public void OnClose(WDBaseEventData data)
        {
            Dismiss();
        }

        // segue/presenting callback from child
        public virtual void ChildWillShow(UIViewController child)
        {
            if(LOG_VERBOSE) Debug.Log("UIViewController: Child will show, "+child.gameObject.name );
        }
        public virtual void ChildDidShow(UIViewController child)
        {
            if(LOG_VERBOSE) Debug.Log("UIViewController: Child did show, "+child.gameObject.name );
        }
        public virtual void ChildWillDismiss(UIViewController child)
        {
           if(LOG_VERBOSE) Debug.Log("UIViewController: Child will dismiss,  "+child.gameObject.name );
        }
        public virtual void ChildDidDismiss(UIViewController child)
        {
           if(LOG_VERBOSE) Debug.Log("UIViewController: Child did dismiss,  "+child.gameObject.name );
        }

        // view (segue) callbacks
        public virtual void OnViewDidAppear()
        {
            if(LOG_VERBOSE) Debug.Log("UIViewController: OnViewDidAppear");
        }
        public virtual void OnViewDidDisappear()
        {
            if(LOG_VERBOSE) Debug.Log("UIViewController: OnViewDidDisappear");
        }
        public virtual void OnViewWillAppear()
        {
            if(LOG_VERBOSE) Debug.Log("UIViewController: OnViewWillAppear");
        }
        public virtual void OnViewWillDisappear()
        {
            if(LOG_VERBOSE) Debug.Log("UIViewController: OnViewWillDisappear");
        }

        // parenting callback on self
        public virtual void WillMove(UIViewController toParent)
        {

        }
        public virtual void DidMove(UIViewController toParent)
        {

        }
        
        // protected
        /// <summary>
        /// Base.Awake() should be called in the derived class
        /// </summary>
        protected virtual void Awake()
        {
            RootView.SetSuperResponder(this);
        }       

        // internal
        internal void SetParentWindow(UIWindow window)
        {
            _ParentWindow = window;
            transform.SetParent(_ParentWindow?.transform);
        }

        internal void SetPresentingParent(UIViewController vc)
        {
            _ParentViewController = vc;
            transform.SetParent(_ParentViewController?.transform);
        }

        internal void SetPresented(UIViewController vc)
        {
            PresentedView = vc;
        }

        // protected
        protected virtual UIViewController PresentedView { get; set; }

        // privates
        private UIWindow _ParentWindow;
        private UIViewController _ParentViewController;

        protected void SendEventViaResponderChain<T>(WDBaseEventData data, EventFunction<T> functor) where T : IEventSystemHandler
        {
            WDExecuteEvents.ExecuteHierarchy<T>( Super,
                                        data,
                                        functor);
        }

        
    }
}