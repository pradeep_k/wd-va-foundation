﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-27 19:47:03

using System;
using System.Collections.Generic;
using System.Linq;
using Framework.EventSystems;
using Framework.UI.ViewController.Navigation;
using UnityEngine;

namespace Framework.UI
{
    public class UINavigationController : UIContainerController, INavigationEventHandler
    {
        // editor
        [SerializeField] UIViewController m_RootViewController;
        [SerializeField] bool m_DismissSelfIfPopOnLastItem = false;

        public UIViewController Current { get { return _stack.Count > 0 ? _stack.Peek() : null; } }

        public override void Show(UIViewController vc, 
                                    UITransitionStyle transition = UITransitionStyle.UseEditorValue,
                                    UIPresentationStyle style = UIPresentationStyle.None)
        {
            Push(vc, transition, style);
        }

        public void Push(UIViewController vc,
                            UITransitionStyle transition = UITransitionStyle.UseEditorValue,
                            UIPresentationStyle style = UIPresentationStyle.None)
        {
            if (_stack.Contains(vc))
                throw new ArgumentException("UINavigationController: Cannot re-add vc that is already part of the stack");

            AddChild(vc, transition, style);
        }

        /// <summary>
        /// Pops till the vc is the top on stack
        /// </summary>
        /// <param name="vc"></param>
        public void PopTo(UIViewController vc, UITransitionStyle transition = UITransitionStyle.UseEditorValue)
        {
            if(vc == null)
                throw new ArgumentNullException("UINavigationController: vc cannot be null");

            if(vc != null && !_stack.Contains(vc))
                throw new ArgumentException("UINavigationController: Cannot pop to vc, "+vc.gameObject.name+" does not exist in stack");
            
            // transit only the top most view
            bool _transit = true;
            while(Current != vc)
            {
                Pop(_transit ? transition : UITransitionStyle.DoNotAnimate);
                _transit = false;
            }
        }

        public void Pop(UITransitionStyle transition = UITransitionStyle.UseEditorValue)
        {
            if(_stack.Count > 1)
            {
                Current.Dismiss(transition);
            }
            else
            {
                if (m_DismissSelfIfPopOnLastItem)
                    Dismiss(transition);
            }
        }

        public override void OnViewWillAppear()
        {
            if (m_RootViewController != null)
                Show(m_RootViewController);
        }

        public override void Dismiss(UITransitionStyle transition = UITransitionStyle.UseEditorValue)
        {
            _stack.Clear();
            base.Dismiss(transition);
        }

        // INavigationEventHandler
        public void OnNavigateBack(WDBaseEventData data)
        {
            Pop();
        }

        public void OnPushToStack(UIViewController vc)
        {
            Push(vc);
        }

        // protected
        /// <summary>
        /// Set will add the view to stack.
        /// Note* setting null will pop the stack. Avoid setting null
        /// </summary>
        /// <value></value>
        protected override UIViewController PresentedView
        {
            get { return Current; }
            set
            {
                if (value != null) 
                {
                    _stack.Push(value);
                    Debug.Log("UINavigationController: Child pushed to stack, " + value.gameObject.name);
                }
                else
                {
                    if (_stack.Count > 0)
                    {
                        Debug.Log("UINavigationController: Child Popped " + Current.gameObject.name);
                        _stack.Pop();
                    }
                }

            }
        }

        // private
        Stack<UIViewController> _stack = new Stack<UIViewController>();

    }
}
