﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-16 19:33:26

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO Remove manual step: attaching view culling to root view.
namespace Framework.UI.Culling
{
    /// <summary>
    /// Disables views that cannot be seen.
    /// </summary>
    [DisallowMultipleComponent()]
    public class UIViewCulling : MonoBehaviour
    {
        // privates
        bool   m_is_canvas;
        UIView m_view;

        Transform ChildRoot{ get{ return m_is_canvas ? transform : m_view.ChildRoot; }}

        void Start()
        {
            m_is_canvas = GetComponent<Canvas>() != null;

            if(!m_is_canvas)
                m_view = GetComponent<UIView>();
        }

        // Update is called once per frame
        void Update()
        {
            if(ChildRoot == null)
                return;

            bool _found_view_that_covers_the_fullscreen = false;
            for(int idx = ChildRoot.childCount - 1; idx >=0; idx--)
            {
                // goes through siblings bottom up
                // looks for a view that is presented in fullscreen
                // disables all views before that, ie. they cannot be seen
                if(ChildRoot.GetChild(idx).gameObject.activeSelf != !_found_view_that_covers_the_fullscreen)
                {
                    ChildRoot.GetChild(idx).gameObject.SetActive(!_found_view_that_covers_the_fullscreen);
                }

                if(!_found_view_that_covers_the_fullscreen)
                {
                    var _view = ChildRoot.GetChild(idx)
                                            .GetComponent<UIView>();
                    _found_view_that_covers_the_fullscreen = _view?.Style == UIPresentationStyle.FullScreen 
                                                            && !UIViewTransitionManager.Instance.IsTransitionInProgress(_view);
                }
            }
        }

    }
}
