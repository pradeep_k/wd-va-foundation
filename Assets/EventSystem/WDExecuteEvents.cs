//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:23:05

using System;
using System.Linq;

using UnityEngine;
using UnityEngine.EventSystems;

using Framework.Responders;
using static UnityEngine.EventSystems.ExecuteEvents;

namespace Framework.EventSystems
{
    /// <remarks>Why not Unity's ExecuteEvents? The main reason for using this is
    /// Unity's components like Selectable don't differentiat/check ISelectHandle call if it is raised by
    /// children or self. This causes issue when responders send event up the hierarchy and unintended unity components react. 
    /// This execute will make sure responder chain events are recieved by only classes that implement IResponder
    /// Note* It is expected for whoever is implementing IResponders should check where the event is coming from
    /// and handle it wisely</remarks>
    public static class WDExecuteEvents
    {   
        #if LOG_WD_EXECUTE_EVENT_VERVOSE
        public const bool LogVerbose = true;
        #else
        public const bool LogVerbose = false;
        #endif


        /// <summary>
        /// Call on all the IResponder attached to the gameobject that can handle the event.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="eventData"></param>
        /// <param name="functor"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static int Execute<T>(GameObject target, WDBaseEventData eventData, EventFunction<T> functor) where T : IEventSystemHandler
        {
            var responders = target.GetComponents<IResponder>()
                                    .Where( x => x is T)
                                    .Cast<T>()
                                    .ToList();
            if(LogVerbose) Debug.Log("WDExecuteEvents: Executing event "+typeof(T)+" on g: "+target
                                    +", Responders: "+responders.Count);

            responders.ForEach( x => functor(x, eventData));
            
            return responders.Count;
        }

        /// <summary>
        /// Traverses recursively upward the IResponder hierarchy till it finds a gameobject that can handle the event.static
        /// Once consumed it will not propogate any further
        /// </summary>
        /// <param name="root"></param>
        /// <param name="eventData"></param>
        /// <param name="callbackFunction"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IResponder ExecuteHierarchy<T>(IResponder root, WDBaseEventData eventData, EventFunction<T> callbackFunction) where T : IEventSystemHandler
        {
            IResponder _hit = WDExecuteEvents.FindAResponderRecursivelyUp<T>(root);
            if(_hit != null)
                WDExecuteEvents.Execute<T>( ((MonoBehaviour) _hit).gameObject,
                                            eventData,
                                            callbackFunction);
            return _hit;
        }

        /// <summary>
        /// Traverses up to find a suitable responder
        /// </summary>
        /// <param name="start"></param>
        /// <returns></returns>
        public static IResponder FindAResponderRecursivelyUp<T>(IResponder start)
        {
            IResponder _responder = null;
            IResponder _current = start;
            while(_current != null)
            {
                if(((MonoBehaviour) _current).GetComponent<T>() != null)
                {
                    _responder = _current;
                    break;
                }

                _current = _current.Super;
            }

            return _responder;
        }
    }
}