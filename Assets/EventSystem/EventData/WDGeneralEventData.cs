//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-30 14:26:52

using System;
using System.Linq;

using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.EventSystems
{
    public class WDGeneralEventData<T> : WDBaseEventData
    {
        public T Data;
        public WDGeneralEventData(GameObject sender, T data) : base(sender)
        {
            Data = data;
        }
    }
}