//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-23 17:07:50

using System;
using System.Linq;

using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.EventSystems
{
    public class WDBaseEventData : BaseEventData
    {
        public GameObject sender;
        
        public WDBaseEventData(GameObject sender) : base(EventSystem.current)
        {
            this.sender = sender;
            this.selectedObject = EventSystem.current.currentSelectedGameObject;
        }
    }
}