//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-30 14:26:52

using System;
using System.Linq;

using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.EventSystems
{
    public class WDPointerEventData : WDBaseEventData
    {
        public PointerEventData PointerEventData;
        public WDPointerEventData(GameObject sender, PointerEventData data) : base(sender)
        {
            PointerEventData = data;
        }
    }
}