/**
 * @author PradeepKumar Rajamanickam
 * @email [pradeep@whodat.in]
 * @create date 2019-07-02 14:20:07
 * @modify date 2019-07-02 14:20:07
 * @desc TODOs and TBDs
 */
 
# TODOs
- qol* UIViewCulling to be attached automatically i.e. without the need for manual steps
- refactor* all messenger comp name from UIXXXXXResponse to UIXXXXXMessenger
- refactor* separate concerns: raising events from messenger to separate action comp for,
    - UIActionWithIDResponse
    - UICancelResponse
    - UICloseResponse
    - UIDoneResponse
    Note* breaking change, waiting for rheo to enter maintanence
- cleanup* Cleanup Namespaces

# TBDs
- new* Dbgvis: runtime visualisation for event traversal path (with interception highlight)
- new* Nested VC support
- new* Modal Presentation for nested view controllers
- new* Bi-Directional Responder Chain support
- new* Update transition api's to support more customisable transitions
