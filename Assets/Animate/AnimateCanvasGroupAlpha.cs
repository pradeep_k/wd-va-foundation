﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-29 18:51:58

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Animate
{
    public class AnimateCanvasGroupAlpha: AnimateBase
    {
        // editor
        public float From;
        public float To;

        public new void Tween(float from, float to, float rate = -1)
        {
            From = from;
            To = to;
            base.Tween(0, 1, rate);
        }

        // protected
        protected override void OnValueChange(float t)
        {
            CanvasGroup.alpha = Mathf.Lerp(From, To, t);
            // Debug.Log("AnimateCanvasGroupAlpha: Value Changed "+CanvasGroup.alpha); // debug only
        }

        // private
        bool m_destroy_canvas_group_on_release=false; // if created by animate destroy on release
        CanvasGroup m_canvas_group;

        CanvasGroup CanvasGroup{ get{
                                    if(m_canvas_group == null)
                                        m_canvas_group = GetComponent<CanvasGroup>();
                                    
                                    if(m_canvas_group == null)
                                    {
                                        m_canvas_group = gameObject.AddComponent<CanvasGroup>();
                                        m_destroy_canvas_group_on_release = true;
                                    }

                                    return m_canvas_group; 
                                }}

        void OnDestroy()
        {
            if(m_destroy_canvas_group_on_release)
            {
                if(m_canvas_group != null)
                    Destroy(m_canvas_group);
            }
        }
    }
}