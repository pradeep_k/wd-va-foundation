﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-27 12:20:05


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Animate
{
      public class AnimateScale: AnimateBase
    {
        // editor
        public Vector2 From;
        public Vector2 To;

        public void Tween(Vector2 from, Vector2 to, float rate = -1)
        {
            From = from;
            To = to;
            Tween(0, 1, rate);
        }

        // protected
        protected override void OnValueChange(float t)
        {
            transform.localScale = Vector2.Lerp(From, To, t);
            // Debug.Log("AnimateScale: Value Changed "+transform.localScale); // debug only
        }
    }
}