﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-29 17:21:54

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Animate
{
      public class AnimateAnchorPosition: AnimateBase
    {
        // editor
        public Vector2 From;
        public Vector2 To;

        public void Tween(Vector2 from, Vector2 to, float rate = -1)
        {
            From = from;
            To = to;
            Tween(0, 1, rate);
        }

        // protected
        protected override void OnValueChange(float t)
        {
            if(m_transform == null)
                m_transform = GetComponent<RectTransform>();

            m_transform.anchoredPosition = Vector2.Lerp(From, To, t);
            
            // Debug.Log("AnimateAnchorPosition: Value Changed "+m_transform.anchoredPosition); // debug only
        }

        // private
        RectTransform m_transform;
    }
}