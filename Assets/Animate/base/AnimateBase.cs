﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-29 17:21:54

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Framework.UI.Animate;
using UnityEngine;
using UnityEngine.Events;
using Debug = UnityEngine.Debug;

namespace Framework.Animate
{
    // TODO: Add tween within time
    public class AnimateBase : MonoBehaviour
    {
        // editor
        /// <summary>
        /// Change Per Second
        /// </summary>
        public float Rate = 0.5f;
        public UnityAction<float> OnComplete;

        // Editor Test Only*
        // public float Test_to = 1f;

        // [ContextMenu("Test_Tween")]
        // public void Test_Tween()
        // {
        //     Tween(Test_to);
        // }
        
        // public
        /// <summary>
        /// Value after curve function is applied
        /// </summary>
        /// <returns></returns>
        public float Value      {   get => ApplyCurveFunction(m_t, m_curve); }
        public float Time       {   get => m_t; 
                                    set{
                                            m_t = Mathf.Clamp(value, 0, 1); 
                                            OnValueChange(Value); 
                                        }
                                }

        public void Tween(Func<float, float> curve = null)
        {
            Tween(1, -1, curve);
        }
        /// <param name="to">0-1</param>
        /// <param name="rate">change per second</param>
        public void Tween(float to, float rate = -1, Func<float, float> curve = null)
        {
            Tween(m_t, to, rate, curve);
        }
        /// <param name="from">0-1</param>
        /// <param name="to">0-1</param>
        /// <param name="rate">change per second</param>
        /// <param name="curve">defaults to linear if null</param>
        public void Tween(float from, float to, float rate = -1, Func<float, float> curve = null)
        {
            m_to = Mathf.Clamp(to, 0, 1);
            m_t = from;

            m_dir   = Mathf.Sign(m_to - from);
            m_min   = m_dir == 1 ? from : m_to;
            m_max   = m_dir == 1 ? m_to : from;
            Rate    = rate > 0 ? rate : Rate;
            m_curve = curve;

            // Debug.Log($"AnimateBase: dir {m_dir}, min {m_min}, max {m_max}"); // debug only
            enabled = true;
        }

        // protected
        protected virtual void OnValueChange(float t){}

        // private
        float m_t = 0f; // 0 - 1
        float m_to = 0f;
        float m_dir = 0f;
        float m_min = 0f;
        float m_max = 1f;
        Func<float, float> m_curve;
        
        void OnEnable()
        {
            OnValueChange(Value);
        }

        void Update()
        {
            if (m_t != m_to)
            {
                m_t = m_t + (Rate * m_dir * UnityEngine.Time.smoothDeltaTime);
                m_t     = Mathf.Clamp(m_t, m_min, m_max);

                // Debug.Log("AnimateBase: Value Changed "+ Value
                //             + ", t: " + m_t
                //             + ", f: " + UnityEngine.Time.deltaTime); // debug only
                OnValueChange(Value);
                return;
            }

            OnComplete?.Invoke(m_t);
            enabled = false;
        }

        float ApplyCurveFunction(float t, Func<float, float> curve)
        {
            if(curve == null)
                return CurveFunctions.Linear(t); // fallback to linear
            else
                return m_curve(t);
        }
    }
}