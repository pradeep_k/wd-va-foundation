﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-29 18:51:58

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Animate
{
    public class AnimateAlpha: AnimateBase
    {
        // editor
        public float From;
        public float To;

        public new void Tween(float from, float to, float rate = -1)
        {
            From = from;
            To = to;
            base.Tween(0, 1, rate);
        }

        // protected
        protected override void OnValueChange(float t)
        {
            m_temp_color = Graphic.color;
            m_temp_color.a = Mathf.Lerp(From, To, t);
            Graphic.color = m_temp_color;

            // Debug.Log("AnimateAlpha: Value Changed "+Graphic.color); // debug only
        }

        // private
        Graphic m_graphics;
        Color m_temp_color;

        Graphic Graphic{ get{
                                if(m_graphics == null)
                                    m_graphics = GetComponent<Graphic>();

                                return m_graphics; 
                        }}
    }
}