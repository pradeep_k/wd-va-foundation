//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:09:53

using Framework.EventSystems;
using Framework.Interaction;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIToggleAction : UIView, ITapEventHandler, ISubmitHandler
{
   // editor
   public bool DefaultState = false;
   public GameObject GraphicForOn;
   public GameObject GraphicForOff;
   public string IdForOnAction;
   public string IdForOffAction;

   public bool State { get => !GraphicForOn.activeSelf; }

   protected override void Start()
   {
       Set(DefaultState, false);
   }

    internal override void OnEnable()
    {
        // add dependency
        if(GetComponent<UITap>() == null)
            gameObject.AddComponent<UITap>();

        base.OnEnable();
    }

   public void Set(bool state, bool raise_event)
   {
       // update graphics
       GraphicForOn.gameObject.SetActive(!state);
       GraphicForOff.gameObject.SetActive(state);

       // Debug.Log("UIToggleAction: state changed to "+state+", gameobject: "+gameObject); // debug only*

       if(!raise_event)
        return;

       // fire event
       if(state)
       {
            WDBaseEventData data = new WDBaseEventData(gameObject);
            FireEvent<IActionWithIDEventHandler>(data, (x, y) => { x.OnActionWithID(IdForOnAction, data); });
       }
       else
       {
            WDBaseEventData data = new WDBaseEventData(gameObject);
            FireEvent<IActionWithIDEventHandler>(data, (x, y) => { x.OnActionWithID(IdForOffAction, data); });
       }
   }

    public void OnTap(WDPointerEventData data)
    {
        Set(!State, true);
    }

    public void OnSubmit(BaseEventData eventData)
    {
        Set(!State, true);
    }
}
