/**
 * @author PradeepKumar Rajamanickam
 * @email [pradeep@whodat.in]
 * @create date 2019-03-25 16:02:01
 * @modify date 2019-03-25 16:02:01
 * @desc View Architecture Overview
 */

# View Architecture
View architecture is modelled after IOS View Architecture.
https://developer.apple.com/library/archive/documentation/General/Conceptual/Devpedia-CocoaApp/View%20Hierarchy.html#//apple_ref/doc/uid/TP40009071-CH2-SW3

Primary reason is to enforce clean structure to improve the overall quality of ui code.

## Core Elements
Note* Anytime the term "event" is used in this document, it refers to EventSystem events
https://docs.unity3d.com/Manual/EventSystem.html

![figure] (https://developer.apple.com/library/archive/featuredarticles/ViewControllerPGforiPhoneOS/Art/VCPG_ControllerHierarchy_fig_1-1_2x.png)
Image Courtesy Apple

### Views 
Every thing is a view, e.g. controls, panel, image, button...etc

- View can hold other views and unity ui components
- Nested view are called sub view
- Parent view are called super view
- It can recieve/handle events
- It can raise event on the object it lives on i.e. it should not send events up stream on its own
- View that is linked to View Controller become  RootView in the current hierarcy
- View must have only one super view or view controller

### Responder Chains
Responder chain is a mechanism, by which events are passed up the view hierarchy.

- Components that implement IResponder are considered to be part of response chain
- ResponseMessanger Component on recieving a event, will push it into the response chain i.e. this will propogate up the view hierarchy until it finds a event handler
- Events once consumed on the hierarchy won't propogate further
- They will handle only event that rise from the gameobjects it is attached to until explicity rethrow is enabled
    
### View Controllers
View controller are components that handle higher level logic mostly project specific aspects.

- There is usually one view controller per full screen view
- It will usually be linked to a root view
- There can be nested view controllers if a view has lot of complex sub views where each subview can be controlled by its own view controller

## Dos & Don'ts
- Don't write large monolithic code structures and controllers that are omni controllers.
- Omni controllers are difficult to maintain as the project scales
- Code should have clear seperation of responsibilities.
- The view hierarchy should be modular i.e. views should broken down into sub-views and should be put together in a sensible way
- Each view must abstract the internal workings of the sub-view i.e. higher level views must not directly access it grand-children it should communicate with its parent
- If a control can be used in another project eg. date picker, let the team know we will discuss and spend time to make it into a re-usable template if it makes sense

## References
https://developer.apple.com/library/archive/documentation/General/Conceptual/Devpedia-CocoaApp/Responder.html#//apple_ref/doc/uid/TP40009071-CH1-SW1
https://developer.apple.com/library/archive/featuredarticles/ViewControllerPGforiPhoneOS/TheViewControllerHierarchy.html#//apple_ref/doc/uid/TP40007457-CH33-SW1
https://medium.com/@amyjoscelyn/the-life-cycle-of-a-view-c98f296fd84e
https://developer.apple.com/library/archive/featuredarticles/ViewControllerPGforiPhoneOS/PresentingaViewController.html#//apple_ref/doc/uid/TP40007457-CH14-SW1