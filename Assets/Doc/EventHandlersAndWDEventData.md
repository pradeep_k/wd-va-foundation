/**
 * @author PradeepKumar Rajamanickam
 * @email [pradeep@whodat.in]
 * @create date 2019-03-30 15:43:02
 * @modify date 2019-03-30 15:43:02
 * @desc Event Handlers And WDEventData
 */

#Event Handlers
Below we have all the available events that will be part of the framework and who can handle them

##Window/System Events

- IAppTerminate() - Will kill the app immediately

##View Controller Events

- IClose()                            - Dismisses the super vc
- IDone()                             - Positive response
- ICancel()                           - Negative response
- ISubmit(BaseEventData)              - Submission event with data, 
                                        eg. Form Data (if coming from a form view), SelectedListData (if coming from list selectable view)...etc
- IValueChanged(value)                - In case vc needs to know value change events arising from views
- IActionWithID(identifier)           - General purpose event which will contain an action identifier

##View Events

- IDragXXX()                  - Unity Events
- IPointerXXXX()              - Unity Events
- ISelectXXX()                - Unity Events
- IClickXXXX()                - Unity Events

- ISubmit(BaseEventData)      - Unity Event, raised by input field...etc
- IValueChanged(value)        - Unity Event

- IDone()                     - Completed response. Usually raised by done button.
                                eg. In complex views like forms this will be consumed by form view and ISubmit(FormData) will be raised to the vc;
[Draft]
*WDBaseEventData*

There were number of issues were because of focus stealing when BaseEventData.selectedObject was set. In-order to avoid this. We now have WDBaseEventData. This derives from base event data but has a seperate sender reference which the WD Event System will be using to verify inside responder chain.

I have already provided new call api's with WDBaseEventData param instead of BaseEventData. However there are still responders and event handler that use the old data param. These will be removed overtime.

Usage
WDBaseEventData data = new WDBaseEventData(sender_gameobjec);
FireEvent<T>(data, (x,y) => OnXXXX(data);