/**
 * @author PradeepKumar Rajamanickam
 * @email [pradeep@whodat.in]
 * @create date 2019-05-29 15:49:05
 * @modify date 2019-05-29 15:49:05
 * @desc Form View Overview
 */

# Form View Components

General purpose form components that can be used to setup user input fields and submit fields data as dictionary. Form view will respond to IDoneEvent's and will submit the form data on done raised from a child/self.

*Note Supports nested forms. Form Views can be extended*
  
| Components          |      Description      |
|---------------------|:---------------------:|
| UIFormView          | Root of a form        |
| UIFormInputField    | For unity input field |
| UIFormToggleField   | For unity toggle      |
| UIFormDropDownField | For unity drop down   |