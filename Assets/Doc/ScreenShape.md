/**
 * @author PradeepKumar Rajamanickam
 * @email [pradeep@whodat.in]
 * @create date 2019-05-29 15:56:32
 * @modify date 2019-05-29 15:56:32
 * @desc ScreenShape
 */
[DRAFT]
Component *ScreenShape*
Will monitor the rendering aspect ratio and decide its shape.
Currently Supported shapes: wide (Landscape Left, Landscape Right), tall (Portrait Left, Portrait Right)

Note* It is a singleton. Needs to be added to the scene manually

New* Component *ScreenShapeBasedTransform*
Component that can be attached to ui rect game objects. Depending on the shape you can define PlaceHolderParent (game object)
that it can attach to and apply specific anchor position and dimensions.

Branch:  *origin/feature/screen-shape-layout*

Note* I have figure out a way to dynamically get the ui to transform based on screen aspect in editor. But ran into issue with nested prefabs.

Known Issue:
- Editor mode does not work with nested prefab. Getting it to work with prefabs in edit mode seems to be tricky. The new nested prefab workflow does not allow disconnected workflows anymore i.e. direct manipulation of prefab assets in the scene.