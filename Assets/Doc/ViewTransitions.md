/**
 * @author PradeepKumar Rajamanickam
 * @email [pradeep@whodat.in]
 * @create date 2019-05-29 14:00:34
 * @modify date 2019-05-29 14:00:34
 * @desc View Transitions
 */
#VIEW TRANSITIONS
View architecture now supports transitions when presenting and dismissing views. They can be setup through code or through editor using UITransition component.

##Usage Editor
Add the UITransition component to the respective vc gameobject. Configure transition direction (i.e. in or out), styles and curve. You can have multiple styles in one transition component. They will play in parallel, eg Fade and Explode...etc

_Note* There can be only one "in" transition component and one "out" transition component._

##Usage Code
When calling Show(), PresentAsModal(), Dismiss(). You can use the new option parameter UITransitionStyles transition to set the flags.

_Note* setting style via code will override what is defined in editor._

Below are the supported inbuilt styles and curves.

**Transitions**
_Fade, Explode, Implode, Slide Right, Slide Left,_
_Slide Up, Slide Down_

**Curves**
_Linear, EaseIn, EaseOut, EaseInOut_