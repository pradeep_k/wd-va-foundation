﻿using System.Collections;
using System.Collections.Generic;
using Framework.UI;
using UnityEngine;

namespace Framework.UI.Test.Navigation
{
    public class UIVCTestViewController : UIViewController,
                                            IGotoNextEventHandler,
                                            IDismissSelfEventHandler
    {
        // editor
        [SerializeField] UIViewController m_NextVC;

        public void OnDismissSelf()
        {
            Debug.Log("Received dismiss event");
            Dismiss();
        }

        public void OnGotoNextEvent()
        {
            Debug.Log("Received goto next event");
            ParentViewController.Show(m_NextVC);
            
        }
    }
}
