﻿using System.Collections;
using System.Collections.Generic;
using Framework.EventSystems;
using Framework.Responders;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.UI.Test
{
    public class UITestDismissEventResponse : ResponseMessenger, IPointerDownHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.Log("UITestDismissEventResponse received by " + gameObject.name);
            WDBaseEventData data = new WDBaseEventData(eventData.selectedObject){ selectedObject = eventData.selectedObject };
            SendResponse<IDismissSelfEventHandler>(data, (x, y) => { x.OnDismissSelf(); });
        }
    }

    public interface IDismissSelfEventHandler : IEventSystemHandler
    {
        void OnDismissSelf();
    }
}
