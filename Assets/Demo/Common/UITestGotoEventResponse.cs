﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-28 19:59:02

using System.Collections;
using System.Collections.Generic;
using Framework.EventSystems;
using Framework.Responders;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.UI.Test
{
    public class UITestGotoEventResponse : ResponseMessenger, IPointerDownHandler
    {
        [ContextMenu("Test")]
        public void Fire()
        {
            WDBaseEventData data = new WDBaseEventData(this.gameObject);
            SendResponse<IGotoNextEventHandler>(data, (x, y) => { x.OnGotoNextEvent(); });
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.Log("UITestGotoEventResponse recieved by "+gameObject.name);
            WDBaseEventData data = new WDBaseEventData(eventData.selectedObject){ selectedObject = eventData.selectedObject };
            SendResponse<IGotoNextEventHandler>(data, (x, y) => { x.OnGotoNextEvent(); });
        }
    }

    public interface IGotoNextEventHandler : IEventSystemHandler
    {
        void OnGotoNextEvent();
    }

}
