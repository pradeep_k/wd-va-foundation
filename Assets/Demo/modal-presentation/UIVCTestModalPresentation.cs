﻿using System.Collections;
using System.Collections.Generic;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;

namespace Framework.UI.Test
{
    public class UIVCTestModalPresentation : UIViewController,
                                            IActionWithIDEventHandler
                                            
    {
        // editor
        [SerializeField] UIViewController m_NextVC;

        public void OnActionWithID(string id, WDBaseEventData data)
        {
            switch(id)
            {
                case "NEXT":
                    Debug.Log("UIVCTestModalPresentation: Received goto next action");
                    Show(m_NextVC);
                    break;
                
                case "DISMISS":
                    Debug.Log("UIVCTestModalPresentation: Received dismiss action");
                    Dismiss();
                    break;
                
                case "NEXT_PARTIAL":
                    Debug.Log("UIVCTestModalPresentation: Received goto next (partial) action");
                    Show(m_NextVC);
                    break;

                case "DISMISS_PARTIAL":
                    Debug.Log("UIVCTestModalPresentation: Received dismiss (partial) action");
                    Dismiss();
                    break;
            }
        }
    }
}