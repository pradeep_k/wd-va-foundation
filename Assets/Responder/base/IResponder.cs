//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-25 16:50:08

namespace Framework.Responders
{
    public interface IResponder
    {
        IResponder Super{get;}
    }
}