//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-25 10:55:49

using System;

using UnityEngine;
using UnityEngine.EventSystems;
using static UnityEngine.EventSystems.ExecuteEvents;

using Framework.EventSystems;
using System.Linq;

namespace Framework.Responders
{
    /// <summary>
    /// Response messangers should listen to unity event system events on the
    /// attached game object and pass it into responder chain 
    /// (i.e IResponders heirachy)
    /// Extend and implement if no response messanger for event already exists.
    /// Note* Should never raise events on its own.
    /// Note* Requires a IResponder that is not a ResponseMessenger 
    /// to pass into chain
    /// </summary>
    public class ResponseMessenger: MonoBehaviour, IResponder
    {
        //editor
        [SerializeField] protected bool m_RethrowEvent = false;

        /// <summary>
        /// Gets super from chain
        /// </summary>
        /// <param name="ResponseMessenger"></param>
        /// <typeparam name="IResponder"></typeparam>
        /// <returns></returns>
        public IResponder Super => GetComponents<IResponder>().Where( x => !(x is ResponseMessenger))
                                                             .FirstOrDefault()
                                                             ?.Super;

        protected void SendResponse<T>(WDBaseEventData data, EventFunction<T> functor) where T : IEventSystemHandler
        {
            if(!gameObject.activeInHierarchy)
                return;

            if(Super != null)
            {
                // rethow other messages only if specified
                if(m_RethrowEvent == false && data.sender != gameObject)
                {
                    // Debug.Log("ResponseMessenger: Rethrow refused "+gameObject.name+", sender: "+data.sender.name); // debug only
                    return;
                }

                WDExecuteEvents.ExecuteHierarchy<T>(Super, data, functor);
           }
           else
           {
               Debug.LogError("ResponseMessenger: No IResponder that is not a ResponseMessagers found. Need one IResponder to pass event into chain");
           }
        }        
    }
}