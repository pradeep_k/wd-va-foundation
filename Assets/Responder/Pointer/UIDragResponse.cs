//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-30 14:20:42

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    /// <summary>
    /// WIP/Experimental
    /// </summary>
    public class UIDragResponse : ResponseMessenger, IDragHandler
    {
        public void OnDrag(PointerEventData eventData)
        {
            WDPointerEventData wddata = new WDPointerEventData(gameObject, eventData);
            SendResponse<IDragHandler>(wddata, (x, y) => {x.OnDrag(eventData); });
        }
    }
}