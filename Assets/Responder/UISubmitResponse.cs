//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-25 10:56:03

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    public class UISubmitResponse : ResponseMessenger, ISubmitHandler
    {
        public void OnSubmit(BaseEventData eventData)
        {
            WDBaseEventData data = null;
            if(eventData is WDBaseEventData)
                data = ((WDBaseEventData) eventData);
            else
                data = new WDBaseEventData(eventData.selectedObject){ selectedObject = eventData.selectedObject };

            SendResponse<ISubmitHandler>(data, (x, y) => { x.OnSubmit(data); });
        }
    }
}