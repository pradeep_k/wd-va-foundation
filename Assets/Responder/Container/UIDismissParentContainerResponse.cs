//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-25 10:56:03

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    public class UIDismissParentContainerResponse : ResponseMessenger, IPointerClickHandler, IDismissParentContainerEventHandler
    {
        /// <summary>
        /// Useful if the view is a panel view, where you will need to disable response on click 
        /// </summary>
        /// <param name="data"></param>
        [SerializeField] bool m_OnClick = false;

        public void OnDismissParentContainer(WDBaseEventData data)
        {
            SendResponse<IDismissParentContainerEventHandler>(data, (x, y) => { x.OnDismissParentContainer(data); });
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if(!m_OnClick)
                return;

            WDBaseEventData data = new WDBaseEventData(this.gameObject);
            SendResponse<IDismissParentContainerEventHandler>(data, (x, y) => { x.OnDismissParentContainer(data); });
        }
    }
}