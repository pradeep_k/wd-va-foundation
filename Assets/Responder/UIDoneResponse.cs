//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-25 10:56:03

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    public class UIDoneResponse : ResponseMessenger, IPointerClickHandler, IDoneEventHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            WDBaseEventData data = new WDBaseEventData(gameObject){ selectedObject = gameObject /* work around for unity selectedobject = null bug */ };
            SendResponse<IDoneEventHandler>(data, (x, y) => { x.OnDone(data); });
        }
        
        public void OnDone(WDBaseEventData data)
        {
            SendResponse<IDoneEventHandler>(data, (x, y) => { x.OnDone(data); });
        }
    }
}