//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-25 10:56:03

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    public class UICloseResponse : ResponseMessenger, IPointerClickHandler, ICloseEventHandler
    {
        /// <summary>
        /// Useful if the view is a panel view, where you will need to disable response on click 
        /// </summary>
        /// <param name="data"></param>
        [SerializeField] bool m_OnClick = false;

        public void OnClose(WDBaseEventData data)
        {
            SendResponse<ICloseEventHandler>(data, (x, y) => { x.OnClose(data); });
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if(!m_OnClick)
                return;

            WDBaseEventData data = new WDBaseEventData(gameObject){ selectedObject = gameObject /* work around for unity selectedobject = null bug */ };
            SendResponse<ICloseEventHandler>(data, (x, y) => { x.OnClose(data); });
        }
    }
}