//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-04 11:59:01

using Framework.EventSystems;
using Framework.Responders;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responder.UI
{
    public class UIValueChangedResponse : ResponseMessenger, IValueChangeEventHandler
    {
        public void OnValueChanged(WDBaseEventData data)
        {
            // Debug.Log("UIValueChangedResponse: value changed, "+data.selectedObject+", self: "+gameObject.name+", type: "+data.GetType()); // debug only*
            SendResponse<IValueChangeEventHandler>(data, (x,y) => { x.OnValueChanged (data); } );
        }
    }
}