
//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-25 10:56:03

using System;
using Framework.EventSystems;
using Framework.UI;
using Framework.UI.ViewController.Navigation;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    public class UINavigateBackResponse : UINavigationItem, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            WDBaseEventData data = new WDBaseEventData(gameObject){ selectedObject = gameObject /* work around for unity selectedobject = null bug */ };
            SendResponse<INavigationEventHandler>(data, (x, y) => { x.OnNavigateBack(data); });
        }
    }
}