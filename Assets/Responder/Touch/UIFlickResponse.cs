//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-30 14:20:42

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    public class UIFlickResponse : ResponseMessenger, IFlickEventHandler
    {
        // rethrow
        public void OnFlick(Vector2 direction, WDPointerEventData data)
        {
            SendResponse<IFlickEventHandler>(data, (x, y) => { x.OnFlick(direction, data); });
        }
    }
}