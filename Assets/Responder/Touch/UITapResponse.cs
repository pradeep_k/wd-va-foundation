//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-30 14:20:42

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    public class UITapResponse : ResponseMessenger, ITapEventHandler
    {
        public void OnTap(WDPointerEventData data)
        {
            SendResponse<ITapEventHandler>(data, (x, y) => { x.OnTap(data); });
        }
    }
}