//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-25 10:56:03

using System;
using Framework.EventSystems;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    // FIXME: Responder should not handle submission event / TODO: Create seperate UISubmitActionWithID
    // FIXME: Responder should not handle click event / TODO: Create seperate UIClickActionWithID
    public class UIActionWithIDResponse : ResponseMessenger, IPointerUpHandler, IActionWithIDEventHandler, ISubmitHandler
    {
        // editor
        [SerializeField] bool OnClick = true;       // TODO: Remove once UIClickActionWithID is ready
        [SerializeField] bool CaptureSubmit = true; // TODO: Remove once UISubmitActionWithID is ready
        [SerializeField] bool BubbleSubmit = true;  // TODO: Remove once UISubmitActionWithID is ready
        [SerializeField] String Identifier;

        public string ButtonIdentifier { get => Identifier; set => Identifier = value; }

        // event capture
        public void OnPointerUp(PointerEventData eventData)
        {
            if(OnClick == false)
                return;
            
            WDBaseEventData data = new WDBaseEventData(gameObject);
            SendResponse<IActionWithIDEventHandler>(data, (x, y) => { x.OnActionWithID(Identifier, data); });
        }

        public void OnSubmit(BaseEventData eventData)
        {
            if(CaptureSubmit == true)
            {
                WDBaseEventData data = new WDBaseEventData(gameObject);
                SendResponse<IActionWithIDEventHandler>(data, (x, y) => { x.OnActionWithID(Identifier, data); });
            }

            if(BubbleSubmit)
            {
                if(transform.parent != null)
                    ExecuteEvents.ExecuteHierarchy<ISubmitHandler>(transform.parent.gameObject, eventData, (x, y) => { x.OnSubmit(y); } );
            }
        }

        // rethrow capture        
        public void OnActionWithID(string id, WDBaseEventData eventData)
        {
            SendResponse<IActionWithIDEventHandler>(eventData, (x, y) => { x.OnActionWithID(id, eventData); });
        }
    }
}