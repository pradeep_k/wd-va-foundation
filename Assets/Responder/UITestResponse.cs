//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-25 10:56:10

using System;
using Framework.EventSystems;
using Framework.Responders;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.Responders.UI
{
    /// <summary>
    /// Test component used for testing responder chain.
    /// Note* Raising event only for test purpose. ResponseMessangers
    /// should not raise events on their own
    /// </summary>
    public class UITestResponse : ResponseMessenger, ITestHandler
    {
        public void OnTestEventRecieved(WDBaseEventData data)
        {
            Debug.Log($"UITestResponse: Test response recieved from {data.selectedObject.name} by {gameObject.name}"
                       +$"\n Rethrow: {m_RethrowEvent}");
            SendResponse<ITestHandler>(data, (x, y) => { x.OnTestEventRecieved(data); });
        }

        [ContextMenu("Test")]
        public void Test()
        {
            WDBaseEventData data = new WDBaseEventData(gameObject);
            OnTestEventRecieved(data);
        }
    }

    
}

namespace Framework.Responders
{
    public interface ITestHandler : IEventSystemHandler
    {
        void OnTestEventRecieved(WDBaseEventData data);
    }
}