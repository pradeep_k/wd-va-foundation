﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:21:32

using System.Collections;
using System.Collections.Generic;
using Framework.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Framework.UI
{
    public sealed class UINavigateView : UIView
    {
        // editor
        [SerializeField] bool m_AutoSelectTopChild = true;
        [SerializeField] bool   m_DonotHideIfLastItem;
        [SerializeField] Button m_Back;

        int m_prev_child_count = 0;
        UIView m_prev_top_view;

        internal override void OnEnable()
        {
            m_prev_top_view = null;
            m_prev_child_count = 0;

            base.OnEnable();
        }

        void Update()
        {
            // hide if this is the last item in the navigation stack, unless explicity mentioned
            if(!m_DonotHideIfLastItem)
                m_Back?.gameObject.SetActive(ChildRoot.childCount > 1);

            // any change in children
            if(m_prev_child_count == ChildRoot.childCount)
                return;

            UIView _top_view = TopView;
            if (m_AutoSelectTopChild)
            {
                FirstSelected = _top_view != null ? _top_view.gameObject.GetComponent<Selectable>() : null;
                if (_top_view != null)
                {
                    EventSystem.current.SetSelectedGameObject(_top_view.gameObject);
                    if (LOG_VERBOSE) Debug.Log("UINavigateView: Selecting child " + _top_view + ", parent: " + this + ", f: " + Time.frameCount);
                }
            }

            // trigger OnFocus
            if(m_prev_top_view != _top_view)
            {
                m_prev_top_view?.OnFocus(false);
                _top_view?.OnFocus(true);

                m_prev_top_view = _top_view;
            }

            m_prev_child_count = ChildRoot.childCount;
        }

        internal override void OnFocus(bool focus)
        {
            if(InFocus == focus)
                return;

            UpdateInFocusVariable(focus);
            if(focus)
            {
                OnCameIntoFocus();
            }
            else
            {
                OnWentOutOfFocus();
            }

            // only top view should receive OnFocus for UINavigateView
            TopView?.OnFocus(focus);
        }
    }
}
