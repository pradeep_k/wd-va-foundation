//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-22 11:34:20

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using System;
using System.Collections;
using Framework.Responders;
using Framework.UI.Culling;
using static UnityEngine.EventSystems.ExecuteEvents;
using Framework.EventSystems;
using System.Collections.Generic;

namespace Framework.UI
{
    /// <summary>
    /// Note* Order in unity inspector matters. Make sure it is always higher than Selectable and UISelectableResponse
    /// </summary>
    [DisallowMultipleComponent()]
    public class UIView : UIBehaviour, IResponder, ISelectHandler
    {
        // editor
        [SerializeField] Selectable m_FirstSelected;
        [Tooltip("Optional* Provide a child transform as root for child views")]
        [SerializeField] Transform  m_CustomChildRoot;
        [SerializeField] UIPresentationStyle m_PresentingStyle = UIPresentationStyle.FullScreen;
        
        // test
        [ContextMenu("TestSelect")]
        public void TestSelect()
        {
            GetComponent<Selectable>()?.Select();
        }

        // publics
        #if LOG_VIEW_ARCH_VERBOSE
        public const bool LOG_VERBOSE = true;
        #else
        public const bool LOG_VERBOSE = false;
        #endif

        public bool InFocus{ get => _in_focus; }
        public bool IsRoot => Super == null || Super is UIViewController;
        public IResponder Super { get => _super != null ? _super : transform.parent?.GetComponentInParent<UIView>(); }
        /// <summary>
        /// Returns custom child root if assigned. If empty current transform will be used.
        /// </summary>
        public Transform ChildRoot { get => m_CustomChildRoot != null ? m_CustomChildRoot : transform; }
        public Selectable FirstSelected { get => m_FirstSelected; set => m_FirstSelected = value; }

        public UIPresentationStyle Style { get{ return m_PresentingStyle; } internal set{ m_PresentingStyle = value; }}

        /// <summary>
        /// Always call Base.Awake()
        /// </summary>
        internal new virtual void Awake()
        {
            if(Super == null)
                gameObject.SetActive(false);
        }

        public void OnSelect(BaseEventData eventData)
        {
            bool _is_sub_view = eventData.selectedObject != gameObject;

            if(_is_sub_view)
            {
                _prev_selected_sub_selectable = eventData.selectedObject.GetComponent<Selectable>();
                if(LOG_VERBOSE) Debug.Log("UIView: Sub-View selected response recieved from "+eventData.selectedObject.name+" by "+gameObject.name+", f: "+Time.frameCount);
            }
            else
            {
                if(LOG_VERBOSE) Debug.Log("UIView: View Selected response recieved from "+eventData.selectedObject.name+" by "+gameObject.name+", f: "+Time.frameCount);
                // FIXME* Cannot recursively call multiple selectable in a single frame
                // highling default selectable
                StartCoroutine(WaitAndDoAfterAFrame(() => {
                    if (m_FirstSelected != null)
                        SelectSubView(m_FirstSelected);
                }));
            }
        }
        
        /// <summary>
        /// Only pass sub views/selectables
        /// </summary>
        /// <param name="selectable"></param>
        public void SelectSubView(Selectable selectable)
        {
            if(LOG_VERBOSE) Debug.Log("UIView: Calling sub view select on "+selectable.name);
            EventSystem.current.SetSelectedGameObject(selectable.gameObject);
        }

        /// <summary>
        /// Send close event to associated view controller
        /// </summary>
        public void Close()
        {
            WDBaseEventData data = new WDBaseEventData(gameObject);
            FireEvent<ICloseEventHandler>(data, (x, y) => { x.OnClose(data); });
        }

        /// <summary>
        /// Gets list of active child views. This is non-recursive
        /// </summary>
        public List<UIView> GetChildViews()
        {
            List<UIView> _views = new List<UIView>();
            foreach (Transform child in ChildRoot)
            {
                var v = child.GetComponent<UIView>();
                if(v != null)
                    _views.Add(v);
            }

            return _views;
        }

        // internals
        /// <summary>
        /// Gets the top most view
        /// </summary>
        internal UIView TopView { get{ return  ChildRoot.childCount > 0 ? 
                                                ChildRoot.GetChild(ChildRoot.childCount - 1).GetComponent<UIView>() 
                                                : null; }}

        
        internal new virtual void OnEnable()
        {
            OnViewEnabled();
        }
        internal new virtual void OnDisable()
        {
            OnViewDisabled();
        }
        
        internal void UpdateInFocusVariable(bool state)
        {
            _in_focus = state;
            if(LOG_VERBOSE)  Debug.Log("UIView: Changed focus to "+_in_focus+", gameobject: "+gameObject+", f: "+Time.frameCount);
        }

        /// Explicitly set super. Will be assigned by view controller 
        /// if this is the root view
        internal void SetSuperResponder(IResponder responder)
        {
            _super = responder;
        }
        /// <summary>
        /// Traverses the tree and returns the potential child selectable that 
        /// will be selected if the current view is selected
        /// </summary>
        /// <remark> Note* This view should have a first selected assigned </remark>
        internal Selectable FindLeafFirstSelectable()
        {
            throw new NotImplementedException();
        }

        internal virtual void OnFocus(bool focus)
        {
            if(_in_focus == focus)
                return;

            UpdateInFocusVariable(focus);
            if(_in_focus)
            {
                OnCameIntoFocus();
            }
            else
            {
                OnWentOutOfFocus();
            }

            // propogate to active children
            GetChildViews().ForEach( x => x.OnFocus(_in_focus));
        }

        // protected
        protected virtual void OnViewEnabled()
        {
            if(LOG_VERBOSE) Debug.Log("UIView: Enabled "+gameObject.name+", f: "+Time.frameCount);
        }
        protected virtual void OnViewDisabled()
        {
            if(LOG_VERBOSE) Debug.Log("UIView: Disabled "+gameObject.name+", f: "+Time.frameCount);
        }
        protected virtual void OnCameIntoFocus()
        {

        }
        protected virtual void OnWentOutOfFocus()
        {

        }

        protected IEnumerator WaitAndDo(Func<bool> predicate, Action callback)
        {
            var wait = new WaitUntil(predicate);
            yield return wait;

            if (callback != null)
                callback();
        }

        /// <returns>No of responders found on the gameobject</returns>
        protected int FireEvent<T>(WDBaseEventData data, EventFunction<T> functor) where T : IEventSystemHandler
        {
            return WDExecuteEvents.Execute<T>(gameObject, data, functor);
        }

        // privates
        bool _in_focus = false;
        IResponder _super; // explicit super responder
        Selectable _prev_selected_sub_selectable;
        
        IEnumerator WaitAndDoAfterAFrame(Action callback)
        {
            yield return null;
            callback?.Invoke();
        }
    }
}