//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:21:01

using UnityEngine;
using UnityEngine.UI;
using Framework.UI.Form;

namespace Framework.UI.Form
{
    [RequireComponent(typeof(Toggle))]
    public class UIFormToggleField : UIFormFieldBase
    {
        internal override void OnEnable()
        {
            GetComponent<Toggle>().onValueChanged.AddListener(OnValueChanged);
            base.OnEnable();
        }

        internal override void OnDisable()
        {
            GetComponent<Toggle>().onValueChanged.RemoveListener(OnValueChanged);
            base.OnDisable();
        }

        public override object Value{
            get => GetComponent<Toggle>().isOn;
            set{ 
                    if(value != null)
                    {
                        // Debug.Log("UIFormToggleField: value updated: "+value); // debug only*
                        GetComponent<Toggle>().isOn = (bool) value; 

                        FormEventData data = new FormEventData(gameObject, Key, value );
                        FireEvent<IValueChangeEventHandler>(data, (x, y) => { x.OnValueChanged(data); });
                    }
                }}

        void OnValueChanged(bool value)
        {
            Value = value;
        }
    }
}