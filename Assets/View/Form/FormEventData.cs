//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:21:14

using System.Collections.Generic;
using Framework.EventSystems;
using UnityEngine;

namespace Framework.UI.Form
{
    public class FormEventData : WDBaseEventData
    {
        public string Key   {get; private set;}
        public object Value {get; private set;}

        public FormEventData(GameObject sender, string key, object value) : base(sender)
        {
            Key = key;
            Value = value;
        }
    }
}