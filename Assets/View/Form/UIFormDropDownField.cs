//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:21:09

using UnityEngine;
using UnityEngine.UI;
using Framework.UI.Form;

namespace Framework.UI.Form
{
    [RequireComponent(typeof(Dropdown))]
    public class UIFormDropDownField : UIFormFieldBase
    {
        internal override void OnEnable()
        {
            GetComponent<Dropdown>().onValueChanged.AddListener(OnValueChanged);
            base.OnEnable();
        }

        internal override void OnDisable()
        {
            GetComponent<Dropdown>().onValueChanged.RemoveListener(OnValueChanged);
            base.OnDisable();
        }

        public override object Value{
            get => GetComponent<Dropdown>().value;
            set{ 
                    if(value != null)
                    {
                        // Debug.Log("UIFormDropDownField: value updated: "+value); // debug only*
                        GetComponent<Dropdown>().value = (int) value; 

                        FormEventData data = new FormEventData(gameObject, Key, value );
                        FireEvent<IValueChangeEventHandler>(data, (x, y) => { x.OnValueChanged(data); });
                    }
                }}

        void OnValueChanged(int value)
        {
            Value = value;
        }
    }
}