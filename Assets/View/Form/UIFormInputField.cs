//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:21:05

using UnityEngine;
using UnityEngine.UI;
using Framework.UI.Form;

namespace Framework.UI.Form
{
    [RequireComponent(typeof(InputField))]
    public class UIFormInputField : UIFormFieldBase
    {
        internal override void OnEnable()
        {
            GetComponent<InputField>().onValueChanged.AddListener(OnValueChanged);
            base.OnEnable();
        }

        internal override void OnDisable()
        {
            GetComponent<InputField>().onValueChanged.RemoveListener(OnValueChanged);
            base.OnDisable();
        }

        public override object Value{
            get => GetComponent<InputField>().text;
            set{ 
                    if(value != null)
                    {
                        // Debug.Log("UIFormInputField: value updated: "+value); // debug only*
                        GetComponent<InputField>().text = (string) value; 

                        FormEventData data = new FormEventData(gameObject, Key, value );
                        FireEvent<IValueChangeEventHandler>(data, (x, y) => { x.OnValueChanged(data); });
                    }
                }}

        void OnValueChanged(string value)
        {
            Value = value;
        }
    }
}