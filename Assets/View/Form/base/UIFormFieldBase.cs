//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:21:20

using UnityEngine;
using UnityEngine.UI;
using Framework.UI.Form;
using System.Collections.Generic;

namespace Framework.UI.Form
{
    public class UIFormFieldBase : UIView
    {
        // editor
        public string Key;

        // publics
        public virtual object Value{get; set;}
        
        public KeyValuePair<string, object> ToKeyValuePair()
        {
            return new KeyValuePair<string, object>(Key, Value);
        }
    }
}