//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:20:56

using System.Collections.Generic;
using System.Linq;
using Framework.EventSystems;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.UI.Form
{
    /// <summary>
    /// Supports nested forms
    /// </summary>
    public class UIFormView : UIFormFieldBase, IDoneEventHandler
    {
        // editor
        public List<UIFormFieldBase> Fields;

        public override object Value{   get => ToDictionary(); 
                                        set{ Debug.LogError("UIFormView: Not supported. Cannot set value for form view"); }}

        public void Submit()
        {
            FormEventData data = new FormEventData(gameObject, Key, Value);
            FireEvent<ISubmitHandler>(data, (x,y) => { x.OnSubmit(data); });
        }
        
        public void OnDone(WDBaseEventData data)
        {
            Submit();
        }

        public UIFormFieldBase Get(string key)
        {
            return Fields.FirstOrDefault(x => x.Key == key);
        }

        public Dictionary<string, object> ToDictionary()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            Fields.ForEach(x => dict.Add(x.Key, x.Value));

            return dict;
        }
    }
}