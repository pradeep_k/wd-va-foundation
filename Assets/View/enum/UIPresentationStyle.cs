//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-05-23 17:06:16

namespace Framework.UI
{
    /// <summary>
    /// How much of the view is it covering
    /// </summary>
    public enum UIPresentationStyle
    {
        FullScreen  = 0,
        Partial     = 1,

        None = -1 // use what ever is set in the storyboard
    }
}