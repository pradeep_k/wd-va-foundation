//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-04 11:59:13

using Framework.EventSystems;
using UnityEngine.EventSystems;

namespace Framework.UI
{
    public interface IValueChangeEventHandler : IEventSystemHandler
    {
        void OnValueChanged(WDBaseEventData eventData);
    }
}