//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-22 13:56:54

using System;
using Framework.EventSystems;
using Framework.Responders;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.UI
{
    public interface IDismissParentContainerEventHandler : IEventSystemHandler
    {
        void OnDismissParentContainer(WDBaseEventData eventData);
    }
}