//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:23:23

using Framework.EventSystems;
using UnityEngine.EventSystems;

namespace Framework.UI
{
    public interface ITapEventHandler : IEventSystemHandler
    {
         void OnTap(WDPointerEventData data);
    }
}