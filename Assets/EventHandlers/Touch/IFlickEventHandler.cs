//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:23:18

using Framework.EventSystems;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.UI
{
    public interface IFlickEventHandler : IEventSystemHandler
    {
         void OnFlick(Vector2 direction, WDPointerEventData data);
    }
}