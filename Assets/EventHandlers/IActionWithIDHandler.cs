﻿//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-30 13:49:29

using System.Collections;
using System.Collections.Generic;
using Framework.EventSystems;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Framework.UI
{
    public interface IActionWithIDEventHandler : IEventSystemHandler
    {
        void OnActionWithID(string id, WDBaseEventData data);
    }
}
