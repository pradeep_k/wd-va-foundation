//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-03-28 22:51:46

using Framework.EventSystems;
using UnityEngine.EventSystems;

namespace Framework.UI.ViewController.Navigation
{
    public interface INavigationEventHandler : IEventSystemHandler
    {
        void OnPushToStack(UIViewController vc);
        void OnNavigateBack(WDBaseEventData data);
    }
}