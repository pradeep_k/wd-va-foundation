//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:23:33

using Framework.EventSystems;
using UnityEngine.EventSystems;

namespace Framework.UI
{
    public interface ICancelEventHandler : IEventSystemHandler
    {
         void OnCancel(WDBaseEventData data);
    }
}