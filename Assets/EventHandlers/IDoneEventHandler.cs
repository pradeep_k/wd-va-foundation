//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-07-02 14:23:48

using Framework.EventSystems;
using UnityEngine.EventSystems;

namespace Framework.UI
{
    public interface IDoneEventHandler : IEventSystemHandler
    {
         void OnDone(WDBaseEventData data);
    }
}