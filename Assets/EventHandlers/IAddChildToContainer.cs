//=============================================================================
//
// Copyright (c) Whodat Tech Pvt Ltd.
//
// All Rights Reserved.
//
// Whodat Confidential and Proprietary
//
//==============================================================================
// author: PradeepKumar Rajamanickam
// 2019-04-22 14:06:24

using System;
using Framework.Responders;
using UnityEngine;
using UnityEngine.EventSystems;
using Framework.UI;

namespace Framework.UI
{
    public interface IAddChildToContainer : IEventSystemHandler
    {
        void OnAddChildViewController(BaseEventData data, UI.UIViewController vc);
    }
}